#include "chess.h"
#include <stdio.h>
#include <stdlib.h>


char invColor(char c){
    switch(c){
        case '_': return '=';
        case '=': return '_';
        case '.': return '@';
        case '@': return '.';
        case ' ': return ' ';
    }

    return '#';
}

char** reverse(char** sqr){
    int filas, columnas, i, j;

    for(filas = 0; sqr[filas] ; filas++);
    for(columnas = 0; sqr[0][columnas]; columnas++);

    char** aux = (char**)malloc(sizeof(char*)*filas);
    int x;

    for(x = 0; x < filas; x++){
        aux[x] = (char*)malloc(sizeof(char)*columnas);
    }
    
    for(i = 0; i < filas; i++){
        for(j = 0; j < columnas; j++){
            aux[i][j] = invColor(sqr[i][j]);
        }
        aux[i][j] = 0;
    }
    aux[i] = 0;

    return aux;
}

char** join(char** s1, char** s2){
    int f1, f2, c1, c2, i, j, k, x;

    for(f1 = 0; s1[f1] ; f1++);
    for(c1 = 0; s1[0][c1]; c1++);

    for(f2 = 0; s2[f2] ; f2++);
    for(c2 = 0; s2[0][c2]; c2++);


    char** aux = (char**)malloc(sizeof(char*)*f1);
    for(x = 0; x < f1; x++){
        aux[x] = (char*)malloc(sizeof(char)*(c1+c2));
    }

    for(i = 0; i<f1; i++){
        for(j = 0; j < c1; j++){
            aux[i][j] = s1[i][j];
        }
        for(k = 0; j < (c1+c2); k++, j++){
            aux[i][j] = s2[i][k];
        }
        aux[i][j] = 0;
    }

    aux[i] = 0;

    return aux;
}


char** flipV(char** sqr){int cont1, cont2;
    for(cont1=0; sqr[cont1] ; cont1++);
    for(cont2 = 0; sqr[0][cont2]; cont2++);

    char** temp = (char**)malloc(sizeof(char*)*(cont1));
  	char* linea=(char*)malloc(sizeof(char)*cont2);

    int x;

    for(x = 0;x<cont1; x++){
      temp[x] = (char*)malloc(sizeof(char)*cont2);
    }
    temp[x] = 0;

  	int i,k;
  	for(i=0;sqr[i];i++){
        linea=sqr[i];
        for(k=0;k<cont2;k++){
            temp[i][cont2-k]=linea[k];
        }
        temp[i][k]=0;
    }
    temp[i]=0;

    return temp;
}

char** flipH(char** sqr){
    int cont1, cont2, x, i, k;

    for(cont1=0; sqr[cont1] ; cont1++);
    for(cont2 = 0; sqr[0][cont2]; cont2++);

    char** temp = (char**)malloc(sizeof(char*)*(cont1));
    for(x = 0;x<cont1; x++){
      temp[x] = (char*)malloc(sizeof(char)*cont2);
    }
	
  	for(i=0;sqr[i];i++){
  		for(k=0;k<cont2;k++){
  			temp[i][k]=sqr[cont1-i-1][k];
  		}
  	temp[i][k]=0;
  	}
  	temp[i]=0;

    return temp;
}

char** rotateR(char** sqr){
    int filas, columnas, x;
	
  	for(filas = 0;sqr[filas];filas++);
  	for(columnas = 0;sqr[0][columnas];columnas++);
	
  	char** aux = (char**)malloc(sizeof(char*)*columnas);

    for(x = 0; x < columnas; x++){
    	aux[x] = (char*)malloc(sizeof(char)*filas);
    }

  	int i, j, k;

  	for(i=0;i<filas;i++){
  		for(j=0;j<columnas;j++){
  			aux[j][i]=sqr[i][j];
  		}		
  	}

  	for(k=0;k<columnas;k++){
        aux[k][filas]=0;
        aux[columnas]=0;
  	}
  	aux = flipV(aux);

  	return aux;

}

char** rotateL(char** sqr){
  	int filas, columnas;
	
  	for(filas = 0;sqr[filas];filas++);
  	for(columnas = 0;sqr[0][columnas];columnas++);
	
  	char** aux = (char**)malloc(sizeof(char*)*columnas);

    int x;

    for(x = 0; x < columnas; x++){
    	aux[x] = (char*)malloc(sizeof(char)*filas);
    }

  	int i, j, k;
  	for(i=0;i<filas;i++){
  		for(j=0;j<columnas;j++){
  			aux[j][i]=sqr[i][j];
  		}		
  	}
  	for(k=0;k<columnas;k++){
        aux[k][filas]=0;
        aux[columnas]=0;
  	}
  	aux = flipH(aux);

  	return aux;
}

char** superImpose(char** s1, char** s2){
    int filas, columnas;

    for(filas = 0; s1[filas] ; filas++);
    for(columnas = 0; s1[0][columnas]; columnas++);

    char** aux = (char**)malloc(sizeof(char*)*filas);
    int x;
    for(x = 0; x < filas; x++){
        aux[x] = (char*)malloc(sizeof(char)*columnas);
    }

    int i, j;
    for(i = 0; i < filas; i++){
        j = 0;
        while(j < columnas){
            if(s2[i][j] == ' '){
                aux[i][j] = s1[i][j];
                j++;
                continue;
            }
            aux[i][j] = s2[i][j];
            j++;
        }
    }
    aux[i] = 0;

    return aux;
}

char** up(char** sqr,char ** sqr1){
    int cont1, cont2, cont3, cont4;

    for(cont1=0; sqr[cont1] ; cont1++);
    for(cont2 = 0; sqr[0][cont2]; cont2++);

    for(cont3=0; sqr[cont3] ; cont3++);
    for(cont4 = 0; sqr[0][cont4]; cont4++);

    char** temp = (char**)malloc(sizeof(char*)*(cont1+cont3));

    int x;
    for(x = 0;x<(cont1+cont3); x++){
      temp[x] = (char*)malloc(sizeof(char)*cont2);
    }

    int i,k;
  	for(i=0;sqr[i];i++){
        for(k=0;k<cont2;k++){
            temp[i][k]=sqr[i][k];
        }
        temp[i][k]=0;
    }

  	int ll;
    for(ll=0;sqr1[ll];i++,ll++){
        for(k=0;k<cont2;k++){
            temp[i][k]=sqr1[ll][k];
        }
        temp[i][k]=0;
    }
    temp[i]=0;
  
    return temp;
}


char** repeatH(char** sqr, int c){
    int filas, columnas, i, j=0, f, k,a;
	
  	for(filas = 0; sqr[filas]; filas++);
  	for(columnas = 0; sqr[0][columnas]; columnas++);
	
  	char** aux = (char**)malloc(sizeof(char*)*filas);
  	int x;
  	for(x = 0; x < filas; x++){
  		aux[x] = (char*)malloc(sizeof(char)*(columnas*c));
  	}

  	int v = 0;
  	for(f = 0; f < c; f++){		
        for(i = 0; i < filas; i++){
            for(j = v, a = 0; a < columnas ; j++, a++){
                aux[i][j] = sqr[i][a];					
            }		
        }
        v = v + columnas;		
    }
  
    for(columnas = 0; aux[0][columnas]; columnas++);
    for(i = 0; i<filas; i++)
        aux[i][columnas-2]=0;

    aux[filas]=0;
		
		return aux;
}

char** repeatV(char** sqr, int n){
    int filas, columnas, x, i, a;
    int j = 0;

    for(filas = 0; sqr[filas] ; filas++);
    for(columnas = 0; sqr[0][columnas]; columnas++);

    char** aux = (char**)malloc(sizeof(char*)*(filas*n));

    for(x = 0; x < filas; x++){
        aux[x] = (char*)malloc(sizeof(char)*columnas);
    }

    for(a = 0; a < n; a++){
        for(i = 0; i < filas; i++, j++){
            aux[j] = sqr[i];
        }
        aux[j] = 0;
    }

    return aux;
}
